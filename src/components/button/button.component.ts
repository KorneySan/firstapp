import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'my-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})

export class ButtonComponent {
    public counter: number = 0;
    @Input() inputText: string = 'Нажата';

    @Output() counterChanged: EventEmitter<number> = new EventEmitter();

    public incrementCounter():void {
        this.counter++;
        this.counterChanged.next(this.counter);
    }
}