import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {
    public button1text: string = 'Нажали';
    public button2text: string = 'Надавили';

    public sum: number = 0;

    public onCounterangedHandler(count: number):void {
        //console.log('Кнопка нажата:', count);
        this.sum++;
    }
}