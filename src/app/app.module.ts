import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ButtonComponent } from 'src/components/button/button.component';

@NgModule({
    imports: [
        BrowserModule
    ],
    declarations: [
        AppComponent,
        ButtonComponent
    ],
    exports: [],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule {

}